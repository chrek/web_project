import re
import getpass

from datetime import datetime
from django.http import HttpResponse

# Create your views here.
username = getpass.getuser()
def home(request):
    return HttpResponse("Welcome, Django!")

def hello_friend(request, name):
    now = datetime.now()
    formatted_now = now.strftime("%A, %d %B, %Y at %X")

    # Filter the name argument to letters only using regular expressions. URL arguments
    # can contain arbitrary text, so we restrict to safe characters only.
    match_object = re.match("[a-zA-Z]+", name)

    if match_object:
        clean_name = match_object.group(0)
    else:
        clean_name = "Friend"      

    content = "Hello " + username + ", my " + clean_name + "! It's " + formatted_now
    return HttpResponse(content)

