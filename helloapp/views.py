from django.shortcuts import render
import getpass

from datetime import datetime
from django.http import HttpResponse

# Create your views here.
username = getpass.getuser()
def home(request):
    #return HttpResponse("Welcome, Django!")
    return render(request, "helloapp/home.html")

def about(request):
    return render(request, "helloapp/about.html")

def contact(request):
    return render(request, "helloapp/contact.html")

def hello_friend(request, name):
    return render(
        request,
        'helloapp/hello_friend.html',
        {
            'name': name,
            'date': datetime.now()
        }
    )