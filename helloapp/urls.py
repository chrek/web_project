from django.urls import path
from . import views

urlpatterns = [
    path("", views.home, name="home"),
    path("helloapp/<name>", views.hello_friend, name="hello_friend"),
    path("about/", views.about, name="about"),
    path("contact/", views.contact, name="contact"),

]
